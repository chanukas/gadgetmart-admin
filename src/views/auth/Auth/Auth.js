import Cookies from 'js-cookie'
import {HOME_PATH} from "../../../routes";


const auth = (props) => {
  if (Cookies.get('logged') === null) {
    props.history.push(HOME_PATH+'/Login');
  }
};

export default auth;
