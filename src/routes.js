import React from 'react';
import DefaultLayout from './containers/DefaultLayout';
import {UserProfile} from "./views/page";
import View from "./views/page/View";

const Home = React.lazy(() => import('./views/page/Home'));
export const HOME_PATH = '/admin'

const routes = [
  { path: HOME_PATH+'/', exact: true, name: '', component: DefaultLayout },
  { path: HOME_PATH+'/Home', name: 'Dashboard', component: Home },
  { path: HOME_PATH+'/user-profile', name: 'Registered Users', component: UserProfile },
  { path: HOME_PATH+'/orders', name: 'All Orders', component: View },
];

export default routes;
