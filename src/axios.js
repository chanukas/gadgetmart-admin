import axios from 'axios';

export const baseUrl = {
  baseURL: 'http://localhost:80',
};

const instance = axios.create({
  baseURL: baseUrl.baseURL
});


export default instance;
