import {HOME_PATH} from "./routes";

export default {
  items: [
    {
      name: 'Home',
      url: HOME_PATH+'/home',
      icon: 'fa fa-Home',
    },
    {
      name: 'System Users',
      url: HOME_PATH+'/user-profile',
      icon: 'fa fa-user',
    },
    {
      name: 'Manage Orders',
      url: HOME_PATH+'/orders',
      icon: 'fa fa-tasks',
    },
  ],
};
